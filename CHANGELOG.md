
## 1.0.4 [07-12-2024]

Add deprecation notice

See merge request itentialopensource/pre-built-automations/cisco-asa-upgrade!16

2024-07-12 18:05:43 +0000

---

## 1.0.3 [05-24-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/cisco-asa-upgrade!14

---

## 1.0.2 [10-20-2022]

* Cypress test updated and fixtures folder created

See merge request itentialopensource/pre-built-automations/cisco-asa-upgrade!11

---

## 1.0.1 [10-19-2022]

* patch/2022-10-18T16-04-14

See merge request itentialopensource/pre-built-automations/cisco-asa-upgrade!9

---

## 1.0.0 [07-19-2022]

* major/2022-07-13T13-28-08

See merge request itentialopensource/pre-built-automations/cisco-asa-upgrade!8

---

## 0.0.4 [06-23-2022]

* moved primary dependency to secondary NSO+IAG

See merge request itentialopensource/pre-built-automations/cisco-asa-upgrade!2

---

## 0.0.3 [06-17-2022]

* Bug fixes and performance improvements

See commit a075a0f

---

## 0.0.2 [06-17-2022]

* Bug fixes and performance improvements

See commit 65017af

---

## 0.0.7 [04-21-2022]

* Bug fixes and performance improvements

See commit 3983a14

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
