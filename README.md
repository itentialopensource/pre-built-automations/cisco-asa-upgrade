<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 04-18-2024 and will be end of life on 04-18-2025. The capabilities of this Pre-Built have been replaced by the [Cisco - ASA - IAG](https://gitlab.com/itentialopensource/pre-built-automations/cisco-asa-iag)

<!-- Update the below line with your Pre-Built name -->
# Pre-Built Name

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

This pre-built contains the Cisco ASA device upgrade workflow for Ansible using IAP.
The workflow requires that a newer binary version file is already downloaded locally on the destination device (bootflash:), and file integrity has been verified (using md5).

This solution consist of the following:

* Main Workflow (**IAP-Artifacts ASA Device Upgrade**)
  * Perform device environmental checks. Verifies a device is on a different version than the requested one. 
  * Perform pre-checks to confirm device readiness.
  * Backup the running-config locally on flash drive.
  * Perform boot statement configuration to direct the router to load the newer version upon the next boot.
  * Issue the reload command.
  * Wait for device to become available after reboot.
  * Confirm reliable connectivity (ping consistency).
  * Perform post-checks to verify the device functionality running the new version.
  * Show a Pre-Post Checks diff report.
  * Perform MOP analysis to verify no unexpected config changes occurred.
  * Show a MOP analysis report.
  * Perform rollback, if requested.

* Command Templates
  * Will run the pre / post commands and evaluate them against set thresholds.

* Analytic Templates
  * Will run the pre vs. post comparisons and evaluate them against set thresholds.

* Automation Catalog Entry with a JSON-Form:
  * Mode selection: Zero-Touch, Normal, or Verbose
  * Allows user to pick destination device to run the upgrade on.
  * Allows user to pick software version to upgrade to (file names are hard coded in JSON form).
  * Ping-consistency variables 

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1`

## Requirements

This Pre-Built requires the following:
* Cisco ASA device is using Ansible as orchestrator
* Download new binary onto destination device (bootflash:)
<!-- Unordered list highlighting the requirements of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * Ansible or NSO (with F5 NED) * -->

## Features

The main benefits and features of the Pre-Built are outlined below.
  * Perform readiness checks prior to any change, and functionality verifications after changes have been applied.
  * Allow for a rollback in case functionality checks have failed.
  * Show a conclusive report with the Pre vs. Post config diff.
<!-- Unordered list highlighting the most exciting features of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## Future Enhancements
* Support for NSO orchestrator
<!-- OPTIONAL - Mention if the Pre-Built will be enhanced with additional features on the road map -->

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built:
Starting an ASA device upgrade can be invoked via Operations Manager. Navigate to the Operations Manager app, then in the Automations section, select Cisco ASA Upgrade. Click the play button to run trigger and enter in the required details in the Cisco ASA Upgrade Form, then Run Manually.
<!-- Explain the main entrypoint(s) for this Pre-Built: Automation Catalog item, Workflow, Postman, etc. -->

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
